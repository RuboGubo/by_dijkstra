graph: dict[str, dict[str, int]] = {
    "A": {"B": 9,  "C": 23},
    "B": {"D": 17, "E": 12, "A": 9 },
    "C": {"G": 5,  "F": 12, "A": 23},
    "D": {"B": 17, "E": 12},
    "E": {"B": 12, "D": 15},
    "F": {"C": 12, "G": 17},
    "G": {"C": 5,  "F": 17, "H": 15, "I": 3},
    "H": {"G": 15, "I": 2 },
    "I": {"G": 3,  "H": 2 , "X": 21},
    "X": {"I": 21}
}

# I could use a float (I think it has infinity) 
# but it does not seem worth the inaccuracy that it introduces
FAKE_INFINITY = 1000000000000000000000000

def dijkstra(graph: dict[str, dict[str, int]], source:str):
    nodes = list(graph.keys())

    distances: dict[str, int] = {}
    previous_node: dict[str, str | None] = {}

    for node in nodes:
        distances[node] = FAKE_INFINITY
        previous_node[node] = None
    distances[source] = 0
    previous_node[source] = source

    while nodes != []:
        selected_vertex = find_small_dist(distances, nodes)
        if selected_vertex != None:
            nodes.remove(selected_vertex)

            for neighbor in list(graph[selected_vertex].keys()):
                if neighbor not in nodes: 
                    continue
                alt_distance = distances[selected_vertex] + graph[selected_vertex][neighbor]
                if alt_distance < distances[neighbor]:
                    distances[neighbor] = alt_distance
                    previous_node[neighbor] = selected_vertex

    print_output(distances, previous_node, source)

def find_small_dist(dist, nodes) -> str | None:
    smallest = (None, FAKE_INFINITY)
    for vertex in dist.items():
        if vertex[1] <= smallest[1] and vertex[0] in nodes:
            smallest = vertex
    return smallest[0]
    
    
def print_output(dist:dict[str, int], prev, source):
    distance_format = ""
    for distance in dist.values():
        distance = str(distance)
        distance_format += padding(distance) + distance
    print(source, distance_format)

def padding(string: str) -> str:
    padding = 3 - len(string)
    return " " * padding

if __name__ == "__main__":
    nodes = list(graph.keys())
    heading = ""
    for node in nodes:
        heading += padding(node) + node
    print(" ", heading)

    for vertex in nodes:
        dijkstra(graph, vertex)